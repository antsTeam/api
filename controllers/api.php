<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('api_model');
    }

    public function index()
    {
        show_404();
    }

    public function cate($id)
    {

    }

      public function video($limit = 10, $offset = 0, $sort = 'id', $dir = 'DESC')
    {
        $pub_key = "da1928593";
        if ($this->input->get('pub_key') != $pub_key) {
            show_404();
        } else {
            $filters = array();
            if ($this->input->get('key')) {
                $filters['title'] = $this->input->get('key', TRUE);
                $filters['description'] = $this->input->get('key', TRUE);
            }
            $filter = "";
            foreach ($filters as $key => $value) {
                $filter .= "&{$key}={$value}";
            }
            $table = 'video';
            $result = $this->api_model->get_all($limit, $offset, $filters, $sort, $dir, $table);
            $total = $result['total'];
            $offset_next = $offset + $limit;
            $offset_previous = $offset-$limit;
            if ($total > $offset_next) {
                $result['next'] = base_url("api/video/" . $limit . '/' . $offset_next . '?pub_key=da1928593');
            } else {
                $result['next'] = null;
            }

            if ($offset_previous < 0) {
                $result['previous'] = null;
            } else {
                $result['previous'] = base_url("api/video/" . $limit . '/' . $offset_previous . '?pub_key=da1928593');
            }
            echo display_json($result);
        }
    }

    public function video_demo($limit = 10, $offset = 0, $sort = 'id', $dir = 'DESC')
    {
        $pub_key = "da1928593";
        if ($this->input->get('pub_key') != $pub_key) {
            show_404();
        } else {
            $offset_next = $offset + $limit;
            $offset_previous = $offset - $limit;
            $filters = array();
            if ($this->input->get('key')) {
                $filters['title'] = $this->input->get('key', TRUE);
                $filters['description'] = $this->input->get('key', TRUE);
            }
            $filter = "";
            foreach ($filters as $key => $value) {
                $filter .= "&{$key}={$value}";
            }
            $table = 'hai_posts';
            $result = $this->api_model->get_all_demo($limit, $offset, $filters, $sort, $dir, $table);

            if ($result['total'] > $limit && $result['total'] > $offset_next) {

                $result['next'] = base_url("api/video/" . $limit . '/' . $offset_next . '?pub_key=' . $pub_key);
            } else {
                $result['next'] = null;
            }

            if ($offset == 0) {
                $result['previous'] = null;
            } else {
                $result['previous'] = base_url("api/video/" . $limit . '/' . $offset_previous . '?pub_key=' . $pub_key);
            }
            echo display_json($result);
        }
    }

    public function videohot($limit = 10, $offset = 0, $sort = 'id', $dir = 'DESC')
    {
        $pub_key = "da1928593";
        if ($this->input->get('pub_key') != $pub_key) {
            show_404();
        } else {
            $offset_next = $offset + $limit;
            $offset_previous = $offset - $limit;
            $filters = array();
            if ($this->input->get('key')) {
                $filters['title'] = $this->input->get('key', TRUE);
                $filters['description'] = $this->input->get('key', TRUE);
            }
            $filter = "";
            foreach ($filters as $key => $value) {
                $filter .= "&{$key}={$value}";
            }
            $table = 'video';
            $result = $this->api_model->get_all_hot($limit, $offset, $filters, $sort, $dir, $table);
            if ($result['total'] > $limit && $result['total'] > $offset_next) {

                $result['next'] = base_url("api/videohot/" . $limit . '/' . $offset_next . '?pub_key=' . $pub_key);
            } else {
                $result['next'] = null;
            }

            if ($offset == 0) {
                $result['previous'] = null;
            } else {
                $result['previous'] = base_url("api/videohot/" . $limit . '/' . $offset_previous . '?pub_key=' . $pub_key);
            }
            echo display_json($result);
        }
    }


    public function templevideo($limit = 10, $offset = 0, $sort = 'id', $dir = 'DESC')
    {
        $pub_key = "da1928593";
        if ($this->input->get('pub_key') != $pub_key) {
            show_404();
        } else {
            $offset_next = $offset + $limit;
            $offset_previous = $offset - $limit;
            $filters = array();
            if ($this->input->get('key')) {
                $filters['title'] = $this->input->get('key', TRUE);
                $filters['description'] = $this->input->get('key', TRUE);
            }
            $filter = "";
            foreach ($filters as $key => $value) {
                $filter .= "&{$key}={$value}";
            }
            $table = 'crawler';
            $result = $this->api_model->get_all($limit, $offset, $filters, $sort, $dir, $table);
            if ($result['total'] > $limit && $result['total'] > $offset_next) {

                $result['next'] = base_url("api/templevideo/" . $limit . '/' . $offset_next);
            } else {
                $result['next'] = null;
            }

            if ($offset == 0) {
                $result['previous'] = null;
            } else {
                $result['previous'] = base_url("api/templevideo/" . $limit . '/' . $offset_previous);
            }
            echo display_json($result);
        }
    }


    public function crawler($limit = 10, $offset = 0, $sort = 'id', $dir = 'DESC')
    {
        $filters = array();
        if ($this->input->get('key')) {
            $filters['title'] = $this->input->get('key', TRUE);
            $filters['description'] = $this->input->get('key', TRUE);
        }
        $table = 'crawler';
        $filter = "";
        foreach ($filters as $key => $value) {
            $filter .= "&{$key}={$value}";
        }
        $result = $this->api_model->get_all($limit, $offset, $filters, $sort, $dir, $table);
        echo display_json($result);
    }
    public function demo($limit = 10, $offset = 0){
        $result = $this ->api_model->get_demo_data($limit,$offset);
        echo display_json($result);
    }
}