<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('video_m');
    }

    public function  index()
    {
        show_404();
    }

    public function vList($limit = 10, $offset = 0)
    {
        $pub_key = "da1928593";
        if ($this->input->get('pub_key') != $pub_key) {
            show_404();
//            echo "hanoi";
        } else {
            $offset_next = $offset + $limit;
            $offset_previous = $offset - $limit;
            $result = $this->video_m->get_demo_data($limit, $offset);
            if ($result['total'] > $limit && $result['total'] > $offset_next) {

                $result['next'] = base_url("/v1/video/vList/" . $limit . '/' . $offset_next . '?pub_key=' . $pub_key);
            } else {
                $result['next'] = null;
            }

            if ($offset == 0) {
                $result['previous'] = null;
            } else {
                $result['previous'] = base_url("/v1/video/vList/" . $limit . '/' . $offset_previous . '?pub_key=' . $pub_key);
            }
            echo display_json($result);
        }
    }
    
     public function GetVideoAll($limit = 10, $offset = 0)
    {
        $pub_key = "da1928593";
        if ($this->input->get('pub_key') != $pub_key) {
            show_404();
//            echo "hanoi";
        } else {
            $offset_next = $offset + $limit;
            $offset_previous = $offset - $limit;
            $result = $this->video_m->get_new_by_type($limit, $offset,0);
            if ($result['total'] > $limit && $result['total'] > $offset_next) {

                $result['next'] = base_url("/v1/video/GetVideoAll/" . $limit . '/' . $offset_next . '?pub_key=' . $pub_key);
            } else {
                $result['next'] = null;
            }

            if ($offset == 0) {
                $result['previous'] = null;
            } else {
                $result['previous'] = base_url("/v1/video/GetVideoAll/" . $limit . '/' . $offset_previous . '?pub_key=' . $pub_key);
            }
            echo display_json($result);
        }
    }
    
      public function GetVideoHot($limit = 10, $offset = 0)
    {
        $pub_key = "da1928593";
        if ($this->input->get('pub_key') != $pub_key) {
            show_404();
//            echo "hanoi";
        } else {
            $offset_next = $offset + $limit;
            $offset_previous = $offset - $limit;
            $result = $this->video_m->get_hot_by_type($limit, $offset,0);
            if ($result['total'] > $limit && $result['total'] > $offset_next) {

                $result['next'] = base_url("/v1/video/GetVideoHot/" . $limit . '/' . $offset_next . '?pub_key=' . $pub_key);
            } else {
                $result['next'] = null;
            }

            if ($offset == 0) {
                $result['previous'] = null;
            } else {
                $result['previous'] = base_url("/v1/video/GetVideoHot/" . $limit . '/' . $offset_previous . '?pub_key=' . $pub_key);
            }
            echo display_json($result);
        }
    }
       public function GetAllGif($limit = 10, $offset = 0)
    {
        $pub_key = "da1928593";
        if ($this->input->get('pub_key') != $pub_key) {
            show_404();
//            echo "hanoi";
        } else {
            $offset_next = $offset + $limit;
            $offset_previous = $offset - $limit;
            $result = $this->video_m->get_gif_all($limit, $offset);
            if ($result['total'] > $limit && $result['total'] > $offset_next) {

                $result['next'] = base_url("/v1/video/GetAllGif/" . $limit . '/' . $offset_next . '?pub_key=' . $pub_key);
            } else {
                $result['next'] = null;
            }

            if ($offset == 0) {
                $result['previous'] = null;
            } else {
                $result['previous'] = base_url("/v1/video/GetAllGif/" . $limit . '/' . $offset_previous . '?pub_key=' . $pub_key);
            }
            echo display_json($result);
        }
    }

     public function GetHot($limit = 10, $offset = 0)
    {
        $pub_key = "da1928593";
        if ($this->input->get('pub_key') != $pub_key) {
            show_404();
//            echo "hanoi";
        } else {
            $offset_next = $offset + $limit;
            $offset_previous = $offset - $limit;
            $result = $this->video_m->get_hot_by_type($limit, $offset,null);
            if ($result['total'] > $limit && $result['total'] > $offset_next) {

                $result['next'] = base_url("/v1/video/GetHot/" . $limit . '/' . $offset_next . '?pub_key=' . $pub_key);
            } else {
                $result['next'] = null;
            }

            if ($offset == 0) {
                $result['previous'] = null;
            } else {
                $result['previous'] = base_url("/v1/video/GetHot/" . $limit . '/' . $offset_previous . '?pub_key=' . $pub_key);
            }
            echo display_json($result);
        }
    }
	
	 public function GetHotGif($limit = 10, $offset = 0)
    {
        $pub_key = "da1928593";
        if ($this->input->get('pub_key') != $pub_key) {
            show_404();
//            echo "hanoi";
        } else {
            $offset_next = $offset + $limit;
            $offset_previous = $offset - $limit;
            $result = $this->video_m->get_hot_by_type($limit, $offset,1);
            if ($result['total'] > $limit && $result['total'] > $offset_next) {

                $result['next'] = base_url("/v1/video/GetHotGif/" . $limit . '/' . $offset_next . '?pub_key=' . $pub_key);
            } else {
                $result['next'] = null;
            }

            if ($offset == 0) {
                $result['previous'] = null;
            } else {
                $result['previous'] = base_url("/v1/video/GetHotGif/" . $limit . '/' . $offset_previous . '?pub_key=' . $pub_key);
            }
            echo display_json($result);
        }
    }


//Admin
    function update($id = 0)
    {
        if ($_POST) {
            if ($id != 0) {
                $user = $this->video_model->get_a_video($id);
                if (!$user) {

                } else {
                    $this->form_validation->set_rules('title', 'Title ', 'trim|required|callback__check_title[' . $user['title'] . ']');
                    $this->form_validation->set_rules('url', 'Video link ', 'trim|required|callback__check_url[' . $user['url'] . ']');
                }
            } else {
                $this->form_validation->set_rules('title', 'Title ', 'trim|required|callback__check_title[]');
                $this->form_validation->set_rules('url', 'Video link ', 'trim|required|callback__check_url[]');
            }
            $this->form_validation->set_rules('image', 'Image link', 'trim|required');
            if ($this->form_validation->run() === FALSE) {
                $result = array('status' => 0, 'desc' => validation_errors());
            } else {
                $data = array(
                    'post_title' => $this->input->post('title'),
                    'post_url' => $this->input->post('url'),
                    'post_image' => $this->input->post('image'),
                    'post_excerpt' => $this->input->post('description'),
                    'post_content' => "https://www.youtube.com/watch?v=" . trim($this->input->post('url')),
                    'post_status' => 'publish',
                );
                if ($id == 0) {
                    $data['post_type'] = "post";
                    $data['post_date'] = date('Y-m-d H:i:s');
                    $data['post_date_gmt'] = date('Y-m-d H:i:s');
                    $data['comment_status'] = "closed";
                    if ($this->video_m->insert($data)) {
                        $result = array('status' => 1, 'desc' => "Success insert");
                    } else {
                        $result = array('status' => 0, 'desc' => 'Error insert');
                    }
                } else {
                    if ($this->video_m->update($id, $data)) {
                        $result = array('status' => 1, 'desc' => "Success update");
                    } else {
                        $result = array('status' => 0, 'desc' => 'Error update, maybe ID not exist');
                    }
                }
            }
        } else {
            $result = array('status' => 0, 'desc' => 'No have post');
        }
        echo display_json($result);
    }

    function delete($id = 0)
    {
        if ($id == 0) {
            $result = array('status' => 0, 'desc' => 'No have post');
        } else {
            if ($this->video_m->delete($id)) {
                $result = array('status' => 1, 'desc' => 'Delete success');
            } else {
                $result = array('status' => 0, 'desc' => 'ID is not exist');
            }
        }
        echo display_json($result);
    }

    function detail($id)
    {
        if ($id == 0) {
            $result = array('status' => 0, 'desc' => 'No have post');
        } else {
            if ($res = $this->video_m->get_a_video($id)) {
                $result = $res;
            } else {
                $result = array('status' => 0, 'desc' => 'ID is not exist');
            }
        }
        echo display_json($result);
    }

//    Private function
    function _check_title($title, $current)
    {
        if (trim($title) != trim($current) && $this->video_m->title_exists($title)) {
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_message('_check_title', sprintf('Title exist', $title));
            return FALSE;
        } else {
            return $title;
        }
    }

    function _check_url($url, $current)
    {
        if (trim($url) != trim($current) && $this->video_m->url_exists($url)) {
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_message('_check_url', sprintf('URL exist', $url));
            return FALSE;
        } else {
            return $url;
        }
    }
}