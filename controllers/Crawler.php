<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crawler extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('crawler_model');
    }

    public function index()
    {
		//add url 
        $url = 'http://chatvl.com';
        $this->load->helper('html_dom');
        include_once(APPPATH . '/helpers/simple_html_dom.php');
		
        $html = file_get_html($url);
		$count =0;
        foreach ($html->find('.gag-link') as $element) {
			$count++;
            $source = $element->find('a', 0)->href;
            if (!$this->crawler_model->checkSource($source)) {
                $data = extract_a_url($source);
                $this->crawler_model->insert($data);
            }
        }
		
		echo 'Success scan '. $count . ' videos!';
    }
	
	  public function mychanel()
    {
		//add url 
        $url = 'https://www.youtube.com/channel/UCPCIFQZnz7DJJma8R6ZvbZQ';
        $this->load->helper('html_dom');
        include_once(APPPATH . '/helpers/simple_html_dom.php');
        $html = file_get_html($url);
		$count =0;
        foreach ($html->find('.yt-lockup-dismissable') as $element) {	
		  $count++;
		  try
		    {
			 $getThumb=$element->find('.yt-thumb-clip img',0) ;
			 $getLink = $element-> find('.yt-lockup-content .yt-lockup-title a',0);
			 $getDes= $element->find('.yt-lockup-description',0);
			
			 $output['image'] =$getThumb-> getAttribute('data-thumb');
			 $output['title'] = $getLink->title;
			 $output['url'] = $getLink->href;
			 $output['source'] =$getLink->href; 
			 
			 $getDes = preg_replace('/<div[^>]*?>([\\s\\S]*?)<\/div>/','\\1', $getDes);	
			 $output['description'] =$getDes ;

		     if (!$this->crawler_model->checkSource($output['source'])) {
					  $this->crawler_model->insert($output);
				}
		    }
		    catch(Exception $e){
			
			}
        }
		
		echo 'Success scan '. $count . ' videos!';
    }
    
     public function getChanel()
    {
		//add url 
        //$url = 'https://www.youtube.com/channel/UCPCIFQZnz7DJJma8R6ZvbZQ';
        $url=  $this->input->get('url');
        $this->load->helper('html_dom');
        include_once(APPPATH . '/helpers/simple_html_dom.php');
        $html = file_get_html($url);
		$count =0;
        $result = array('status' => 0, 'desc' =>'crawler fail');
        foreach ($html->find('.yt-lockup-dismissable') as $element) {	
		  $count++;
		  try
		    {
			 $getThumb=$element->find('.yt-thumb-clip img',0) ;
			 $getLink = $element-> find('.yt-lockup-content .yt-lockup-title a',0);
			 $getDes= $element->find('.yt-lockup-description',0);
			
			 $output['image'] =$getThumb-> getAttribute('data-thumb');
			 $output['title'] = $getLink->title;
			 $output['url'] = $getLink->href;
			 $output['source'] =$getLink->href; 
			 
			 $getDes = preg_replace('/<div[^>]*?>([\\s\\S]*?)<\/div>/','\\1', $getDes);	
			 $output['description'] =$getDes ;

		     if (!$this->crawler_model->checkSource($output['source'])) {
					  $this->crawler_model->insert($output);
				}
              $result = array('status' => 1, 'desc' =>'crawler sucess ' .  $count . ' video');
		    }
		    catch(Exception $e){
			 $result = array('status' => 0, 'desc' => "crawler fail");
			}
        }
		 echo display_json($result);
		//echo 'Success scan '. $count . ' videos!';
    }
	
	
	
	 public function getGifReddit()
    {
		//add url 
        $url = 'https://www.reddit.com/r/gifs/';
        $this->load->helper('html_dom');
        include_once(APPPATH . '/helpers/simple_html_dom.php');
        $html = file_get_html($url);
		//echo $html;
		$count =0;
        foreach ($html->find('.thing') as $element) {	
		  $count++;
		  try
		    {
			 //echo $element;
			
			//GET THUMB
			$getThumb = $element-> find('.thumbnail img',0)->src;
			//echo $getThumb;
			
			//GET LINK 
			$getLink= $element->find('.title a',0);
			//echo $getLink->href; 
			$getDes = preg_replace('/<a[^>]*?>([\\s\\S]*?)<\/a>/','\\1', $getLink);	
			//echo $getDes;
			$output['image'] =$getThumb;
			$output['title'] = $getDes;
			$output['url'] = $getLink->href;
			$output['source'] =$getLink->href; 
			
		     // if (!$this->crawler_model->checkSource($output['source'])) {
					  // $this->crawler_model->insert($output);
				// }
		    }
		    catch(Exception $e){
			
			}
        }
		
		echo 'Success scan '. $count . ' videos!';
    }
	
}