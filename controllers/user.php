<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
    }

    public function index()
    {
        show_404();
    }

    public function login()
    {
        if ($this->session->userdata('logged_in')) {
            redirect('admin');
        }
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|callback__check_login');
        if ($this->form_validation->run() == TRUE) {
            redirect('admin');
        }
        $this->template
            ->set_layout('login')
            ->build('user/login');
    }

    public function logout()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('login');
    }

    function _check_login($password)
    {
        $login = $this->users_model->login($this->input->post('username', TRUE), $password);
        if ($login) {
            $this->session->set_userdata('logged_in', $login);
            return TRUE;
        }
        $this->form_validation->set_message('_check_login', 'Username or password error');
        return FALSE;
    }
}
