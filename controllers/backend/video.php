<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('video_model');
    }

    function index()
    {
        show_404();
    }

    function update($id = 0)
    {
        if ($_POST) {
            if ($id != 0) {
                $user = $this->video_model->get_a_video($id);
                if (!$user) {

                } else {
                    $this->form_validation->set_rules('title', 'Title ', 'trim|required|callback__check_title[' . $user['title'] . ']');
                    $this->form_validation->set_rules('url', 'Video link ', 'trim|required|callback__check_url[' . $user['url'] . ']');
                }
            } else {
                $this->form_validation->set_rules('title', 'Title ', 'trim|required|callback__check_title[]');
                $this->form_validation->set_rules('url', 'Video link ', 'trim|required|callback__check_url[]');
            }
            $this->form_validation->set_rules('image', 'Image link', 'trim|required');
            if ($this->form_validation->run() === FALSE) {
                $result = array('status' => 0, 'desc' => validation_errors());
            } else {
                $data = array(
                    'title' => $this->input->post('title'),
                    'url' => $this->input->post('url'),
                    'image' => $this->input->post('image'),
                    'description' => $this->input->post('description'),
                    'status' => '1',
                );
                if ($id == 0) {
                    $data['created'] = date('Y-m-d H:i:s');
                    if ($this->video_model->insert($data)) {
                        $result = array('status' => 1, 'desc' => "Success insert");
                    } else {
                        $result = array('status' => 0, 'desc' => 'Error insert');
                    }
                } else {
                    if ($this->video_model->update($id, $data)) {
                        $result = array('status' => 1, 'desc' => "Success update");
                    } else {
                        $result = array('status' => 0, 'desc' => 'Error update, maybe ID not exist');
                    }
                }
            }
        } else {
            $result = array('status' => 0, 'desc' => 'No have post');
        }
        echo display_json($result);
    }

    function delete($id = 0)
    {
        if ($id == 0) {
            $result = array('status' => 0, 'desc' => 'No have post');
        } else {
            if ($this->video_model->delete($id)) {
                $result = array('status' => 1, 'desc' => 'Delete success');
            } else {
                $result = array('status' => 0, 'desc' => 'ID is not exist');
            }
        }
        echo display_json($result);
    }

    function detail($id)
    {
        if ($id == 0) {
            $result = array('status' => 0, 'desc' => 'No have post');
        } else {
            if ($res = $this->video_model->get_a_video($id)) {
                $result = $res;
            } else {
                $result = array('status' => 0, 'desc' => 'ID is not exist');
            }
        }
        echo display_json($result);
    }

//    Private function
    function _check_title($title, $current)
    {
        if (trim($title) != trim($current) && $this->video_model->title_exists($title)) {
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_message('_check_title', sprintf('Title exist', $title));
            return FALSE;
        } else {
            return $title;
        }
    }

    function _check_url($url, $current)
    {
        if (trim($url) != trim($current) && $this->video_model->url_exists($url)) {
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_message('_check_url', sprintf('URL exist', $url));
            return FALSE;
        } else {
            return $url;
        }
    }

}