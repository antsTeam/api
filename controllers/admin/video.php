<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('video_model');
    }

    public function index()
    {
        $data = array(
            'modules' => 'video',
            'list' => $this->video_model->get_all()
        );
        $this->template
            ->build('admin/video/video_list', $data);
    }

    function update($id = 0)
    {
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('title', 'Title', 'required|trim');
        $this->form_validation->set_rules('url', 'Video link', 'required|trim');
        $this->form_validation->set_rules('image', 'Image link', 'required|trim');
        if ($this->form_validation->run() == TRUE) {
            $data = array(
                'title' => $this->input->post('title'),
                'url' => $this->input->post('url'),
                'image' => $this->input->post('image'),
                'description' => $this->input->post('description'),
                'status' => '1'
            );
            if ($id != 0) {
                if ($this->video_model->update($id, $data)) {
                    redirect('admin/video');
                }
            } else {
                $data ['created'] = date('Y-m-d H:i:s');
                if ($this->video_model->insert($data)) {
                    redirect('admin/video');
                }
            }
        }
        if ($id != 0) {
            $result = $this->video_model->show($id);
            $result ['modules'] = 'video';
        } else {
            $result = array(
                'modules' => 'video'
            );
        }
        $this->template
            ->build('admin/video/update', $result);
    }

    function delete($id = 0)
    {
        if ($this->video_model->delete($id)) {
            redirect('admin/video');
        }
    }
}
