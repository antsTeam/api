<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $this->template
            ->build('admin/backend/backend'); // views/welcome_message
    }
}
