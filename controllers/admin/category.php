<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $data = array('modules', 'category');
        $this->template
            ->build('admin/category/category', $data); // views/welcome_message
    }

}