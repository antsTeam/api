<?php
if (!function_exists('extract_data_url')) {
    function extract_data_url($url)
    {
        include_once(APPPATH . '/libraries/eac_curl.class.php');
        include_once(APPPATH . '/libraries/leak.php');
        $options = array();
        $options['CURLOPT_AUTOREFERER'] = 1;
        $options['CURLOPT_CRLF'] = 1;
        $options['CURLOPT_NOPROGRESS'] = 1;
        $http = new cURL($options);
        $leek = new leek();
        $http->setOptions($options);
        if (strpos($url, "chatvl.com")) {
            $data = $http->get($url);
            $leek->setSource($data);
            $output['title'] = text_no_html($leek->findField("<h1>", "</h1>"));
            $urlY = trim($leek->findField('<iframe id="video-iframe" class="video-iframe" width="727" height="410" src="', '" frameborder="0" allowfullscreen="1"></iframe>'));
            $urlY = explode('?', $urlY);
            $image = explode('/', $urlY[0]);
            $output['url'] = $urlY[0];
            $output['image'] = 'http://i.ytimg.com/vi/' . end($image) . '/0.jpg';
            $output['description'] = '';
            $output['source'] = $url;
        }
        return $output;
    }
}
if (!function_exists('extract_a_url')) {
    function extract_a_url($url)
    {
        include_once(APPPATH . '/libraries/eac_curl.class.php');
        include_once(APPPATH . '/helpers/simple_html_dom.php');
        $options = array();
        $options['CURLOPT_AUTOREFERER'] = 1;
        $options['CURLOPT_CRLF'] = 1;
        $options['CURLOPT_NOPROGRESS'] = 1;
        $http = new cURL($options);
        $http->setOptions($options);
        $data_url = $http->get($url);
        $html = str_get_html($data_url);
        if (strpos($url, "chatvl.com")) {
            $output['image'] = $html->find('link ', 0)->href;
            $output['title'] = $html->find('h1', 0)->plaintext;
            $out_url = $html->find('iframe', 0)->src;
            $out_url = explode('?', $out_url);
            $output['url'] = $out_url[0];
            $output['source'] = $url;
            $output['description'] = '';
        }
        return $output;

    }
}

if (!function_exists('text_no_html')) {
    function text_no_html($str)
    {
        return trim(strip_tags($str));
    }
}