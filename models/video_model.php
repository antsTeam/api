<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Video_model extends CI_Model
{
    private $_db;

    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_db = 'video';
    }

    function get_all()
    {

        $this->db->select('*');
        $this->db->where('status', '1');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->_db);
        if ($result = $query->result()) {
            return $result;
        } else {
            return false;
        }
    }
	
	 function get_all_hot()
    {

        $this->db->select('*');
        $this->db->where('status', '1');
        $this->db->order_by("id", "desc");
		$this->db->order_by("ishot", "1");
        $query = $this->db->get($this->_db);
        if ($result = $query->result()) {
            return $result;
        } else {
            return false;
        }
    }


    function show($id)
    {
        if ($id) {
            $sql = "
                SELECT *
                FROM {$this->_db}
                WHERE id = " . $this->db->escape($id) . "
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows()) {
                return $query->row_array();
            }
        }
        return false;
    }

    function insert($data = array())
    {
        $this->db->insert($this->_db, $data);
        if ($id = $this->db->insert_id()) {
            return $id;
        } else {
            return false;
        }
    }

    function update($id, $data)
    {
        $this->db->update($this->_db, $data, array('id' => $id));
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function delete($id)
    {
        $this->db->delete($this->_db, array('id' => $id));
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function get_a_video($id = NULL)
    {
        if ($id) {
            $sql = "
                SELECT *
                FROM {$this->_db}
                WHERE id = " . $this->db->escape($id) . "
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows()) {
                return $query->row_array();
            }
        }
        return FALSE;
    }


//    hide
    function title_exists($title)
    {
        $sql = "
            SELECT id
            FROM {$this->_db}
            WHERE title = " . $this->db->escape($title) . "
            LIMIT 1
        ";
        $query = $this->db->query($sql);

        if ($query->num_rows()) {
            return TRUE;
        }
        return FALSE;
    }
    function url_exists($url)
    {
        $sql = "
            SELECT id
            FROM {$this->_db}
            WHERE url = " . $this->db->escape($url) . "
            LIMIT 1
        ";
        $query = $this->db->query($sql);

        if ($query->num_rows()) {
            return TRUE;
        }
        return FALSE;
    }
}