<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();

        // define primary table
    }
	
	
	
	function get_all_hot($limit = 0, $offset = 0, $filters = array(), $sort = 'id', $dir = 'DESC', $table = 'video')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS *
            FROM {$table}
            WHERE status = '1'
			and ishot ='1'
        ";

        if (!empty($filters)) {
            $i = 0;
            foreach ($filters as $key => $value) {
                $i++;
                $value = $this->db->escape('%' . $value . '%');
                if ($i > 1) {
                    $sql .= " OR {$key} LIKE {$value}";
                } else {
                    $sql .= " AND {$key} LIKE {$value}";
                }

            }
        }
        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit) {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $results['results'] = $query->result_array();
        } else {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }

    function get_all($limit = 0, $offset = 0, $filters = array(), $sort = 'id', $dir = 'DESC', $table = 'video')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS *
            FROM {$table}
            WHERE status = '1'
        ";

        if (!empty($filters)) {
            $i = 0;
            foreach ($filters as $key => $value) {
                $i++;
                $value = $this->db->escape('%' . $value . '%');
                if ($i > 1) {
                    $sql .= " OR {$key} LIKE {$value}";
                } else {
                    $sql .= " AND {$key} LIKE {$value}";
                }

            }
        }

        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit) {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $results['results'] = $query->result_array();
        } else {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }
}