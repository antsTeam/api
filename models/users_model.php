<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model
{
    private $_db;

    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_db = 'users';
    }

    function login($username = NULL, $password = NULL)
    {
        if ($username && $password) {
            $this->db->select('name,pass');
            $this->db->where('name', $username);
            $query = $this->db->get($this->_db);
            if ($query->result()) {
                $result = $query->result();
                if (md5($password) == $result[0]->pass) {
//                    $result_return = $result[0];
                    return true;
                }
            } else {
                return false;
            }
        }

        return FALSE;
    }
}