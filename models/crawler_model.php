<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Crawler_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        // define primary table
        $this->_db = 'crawler';
    }

//Check exit;
    function checkSource($src)
    {
	   $sql = "
            SELECT id
            FROM {$this->_db}
            WHERE source = " . $this->db->escape($src) . "
            LIMIT 1
        ";
        $query = $this->db->query($sql);
        if ($query->num_rows()) {
            return TRUE;
        }
        return FALSE;
    }
    function insert($data = array())
    {
        $this->db->insert($this->_db, $data);
        if ($id = $this->db->insert_id()) {
            return $id;
        } else {
            return false;
        }
    }
}