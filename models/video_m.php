<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Video_m extends CI_Model
{
    private $_db;

    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_db = 'hai_posts';
    }
     function  get_demo_data($limit = 0, $offset = 0)
    {
		$sql = 'SELECT
				us.display_name author,
				p1.ID as id,
				p1.post_title as title,
				p1.post_title as description,
				p1.post_content as url,
				p1.post_modified as created,
				wm5.guid image,
				0 as cid, 
				IFNULL(wm3.meta_value,0) as views,
				IFNULL(wm4.post_type,0) as typepost
			FROM 
				hai_posts p1
			LEFT JOIN 
				hai_postmeta wm1
				ON (
					wm1.post_id = p1.id 
					AND wm1.meta_value IS NOT NULL
					AND wm1.meta_key = "_thumbnail_id"              
				)
			left join 
				hai_users us
				on (
						p1.post_author =us.ID 
				 )
			left join 
				hai_postmeta wm3
				on 
				(
					wm3.post_id = p1.id 
				   AND wm3.meta_key = "_count-views_all"
				)
			left join
				(select object_id,1 as post_type from hai_term_relationships where term_taxonomy_id=
				(SELECT term_taxonomy_id FROM hai_term_taxonomy
				where term_id =(SELECT term_id FROM hai_terms where name ="post-format-image")))  wm4
			on (
				wm4.object_id=p1.id 
			)
			left join 
			(select id,guid from hai_posts) wm5
			on 
			(wm1.meta_value=wm5.id)
			WHERE
				p1.post_status="publish" 
				AND p1.post_type="post"
			ORDER BY 
				p1.post_date DESC';
		$sql_litmit = $sql . ' limit ' . $offset . ',' . $limit;
		$query=$this->db->query($sql,false);
		$query_limit=$this->db->query($sql_litmit,false);
        $num_results = $query->num_rows();
		
		
		$pattern_img = '/^\[WPGP\sgif_id="(\d+)".*\]$/i';
		$pattern_video ='#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#';
		$replacement = '$1';
		
	    //get image
		foreach ($query_limit->result() as $row)
		{
			if ($row->url !=null) 
			{
				if (preg_match($pattern_img, $row->url)) {
				  $id_url= preg_replace($pattern_img, $replacement, $row->url);
				  $sql_img = "select guid from hai_posts where id =" . $id_url ;
				  $query_img=$this->db->query($sql_img,false);
				  $row->url= $query_img->result()[0]->guid;
				  
				}
				
				if (preg_match($pattern_video, $row->url,$matches)) {
					$row->url=$matches[0];
                    $row->image ="http://i.ytimg.com/vi/" . $matches[0] . "/0.jpg";
				}
				
			}
		}
        $kd['results'] = $query_limit->result();
        $kd['total'] = $num_results;
        if (!empty($kd)) {
            return ($kd);
        } else {
            return false;
        }
    }
    
     function  get_gif_all($limit = 0, $offset = 0)
    {
		$sql = 'SELECT
				us.display_name author,
				p1.ID as id,
				p1.post_title as title,
				p1.post_title as description,
				p1.post_content as url,
				p1.post_modified as created,
				wm5.guid image,
				0 as cid, 
				IFNULL(wm3.meta_value,0) as views,
				IFNULL(wm4.post_type,0) as typepost
			FROM 
				hai_posts p1
			LEFT JOIN 
				hai_postmeta wm1
				ON (
					wm1.post_id = p1.id 
					AND wm1.meta_value IS NOT NULL
					AND wm1.meta_key = "_thumbnail_id"              
				)
			left join 
				hai_users us
				on (
						p1.post_author =us.ID 
				 )
			left join 
				hai_postmeta wm3
				on 
				(
					wm3.post_id = p1.id 
				   AND wm3.meta_key = "_count-views_all"
				)
			left join
				(select object_id,1 as post_type from hai_term_relationships where term_taxonomy_id=
				(SELECT term_taxonomy_id FROM hai_term_taxonomy
				where term_id =(SELECT term_id FROM hai_terms where name ="post-format-image")))  wm4
			on (
				wm4.object_id=p1.id 
			)
			left join 
			(select id,guid from hai_posts) wm5
			on 
			(wm1.meta_value=wm5.id)
			WHERE
				p1.post_status="publish" 
				AND p1.post_type="post"
                and wm4.post_type=1
			ORDER BY 
				p1.post_date DESC';
		$sql_litmit = $sql . ' limit ' . $offset . ',' . $limit;
		$query=$this->db->query($sql,false);
		$query_limit=$this->db->query($sql_litmit,false);
        $num_results = $query->num_rows();
		
		
		$pattern_img = '/^\[WPGP\sgif_id="(\d+)".*\]$/i';
		$pattern_video ='#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#';
		$replacement = '$1';
		
	    //get image
		foreach ($query_limit->result() as $row)
		{
                $thu= $row->url;
				if ($row->typepost = 1) {
                
				   preg_match($pattern_img, $row->url,$matches);
                   echo $matches[0];
                  // $id_url=0;
				  //$sql_img = "select guid from hai_posts where id =" . $id_url ;
                  
				  //$query_img=$this->db->query($sql_img,false);
				  //$row->url= $query_img->result()[0]->guid;
				  
				}
				
				if (preg_match($pattern_video, $row->url,$matches)) {
					$row->url=$matches[0];
                   $row->image ="http://i.ytimg.com/vi/" . $matches[0] . "/0.jpg";
				}
				

		}
        $kd['results'] = $query_limit->result();
        $kd['total'] = $num_results;
        if (!empty($kd)) {
            return ($kd);
        } else {
            return false;
        }
    }
    
    function  get_new_by_type($limit = 0, $offset = 0,$type=null)
    {
	   
			$sql = 'SELECT
				us.display_name author,
				p1.ID as id,
				p1.post_title as title,
				p1.post_title as description,
				p1.post_content as url,
				p1.post_modified as created,
				wm5.guid image,
				0 as cid, 
				IFNULL(wm3.meta_value,0) as views,
				IFNULL(wm4.post_type,0) as typepost
			FROM 
				hai_posts p1
			LEFT JOIN 
				hai_postmeta wm1
				ON (
					wm1.post_id = p1.id 
					AND wm1.meta_value IS NOT NULL
					AND wm1.meta_key = "_thumbnail_id"              
				)
			left join 
				hai_users us
				on (
						p1.post_author =us.ID 
				 )
			left join 
				hai_postmeta wm3
				on 
				(
					wm3.post_id = p1.id 
				   AND wm3.meta_key = "_count-views_all"
				)
			left join
				(select object_id,1 as post_type from hai_term_relationships where term_taxonomy_id=
				(SELECT term_taxonomy_id FROM hai_term_taxonomy
				where term_id =(SELECT term_id FROM hai_terms where name ="post-format-image")))  wm4
			on (
				wm4.object_id=p1.id 
			)
			left join 
			(select id,guid from hai_posts) wm5
			on 
			(wm1.meta_value=wm5.id)
			WHERE
				p1.post_status="publish" 
				AND p1.post_type="post"
            ' . ' and IFNULL(wm4.post_type,0) = ' . $type .
			' ORDER BY 
				p1.post_date DESC';
		
		$sql_litmit = $sql . ' limit ' . $offset . ',' . $limit;
		$query=$this->db->query($sql,false);
		$query_limit=$this->db->query($sql_litmit,false);
        $num_results = $query->num_rows();
		
		
		$pattern_img = '/^\[WPGP\sgif_id="(\d+)".*\]$/i';
		$pattern_video ='#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#';
		$replacement = '$1';
		
	    //get image
		foreach ($query_limit->result() as $row)
		{
			if ($row->url !=null) 
			{
				if (preg_match($pattern_img, $row->url)) {
				  $id_url= preg_replace($pattern_img, $replacement, $row->url);
				  $sql_img = "select guid from hai_posts where id =" . $id_url ;
				  $query_img=$this->db->query($sql_img,false);
				  $row->url= $query_img->result()[0]->guid;
				  
				}
				
				if (preg_match($pattern_video, $row->url,$matches)) {
					$row->url=$matches[0];
                   $row->image ="http://i.ytimg.com/vi/" . $matches[0] . "/0.jpg";
				}
				
			}
		}
        $kd['results'] = $query_limit->result();
        $kd['total'] = $num_results;
        if (!empty($kd)) {
            return ($kd);
        } else {
            return false;
        }
    }

    
     function  get_hot_by_type($limit = 0, $offset = 0,$type=null)
    {
	    
			$sql = 'SELECT
				us.display_name author,
				p1.ID as id,
				p1.post_title as title,
				p1.post_title as description,
				p1.post_content as url,
				p1.post_modified as created,
				wm5.guid image,
				0 as cid, 
				IFNULL(wm3.meta_value,0) as views,
				IFNULL(wm4.post_type,0) as typepost
			FROM 
				hai_posts p1
			LEFT JOIN 
				hai_postmeta wm1
				ON (
					wm1.post_id = p1.id 
					AND wm1.meta_value IS NOT NULL
					AND wm1.meta_key = "_thumbnail_id"              
				)
			left join 
				hai_users us
				on (
						p1.post_author =us.ID 
				 )
			left join 
				hai_postmeta wm3
				on 
				(
					wm3.post_id = p1.id 
				   AND wm3.meta_key = "_count-views_all"
				)
			left join
				(select object_id,1 as post_type from hai_term_relationships where term_taxonomy_id=
				(SELECT term_taxonomy_id FROM hai_term_taxonomy
				where term_id =(SELECT term_id FROM hai_terms where name ="post-format-image")))  wm4
			on (
				wm4.object_id=p1.id 
			)
			left join 
			(select id,guid from hai_posts) wm5
			on 
			(wm1.meta_value=wm5.id)
			WHERE
				p1.post_status="publish" 
				AND p1.post_type="post"'
				. 'and IFNULL(wm4.post_type,0)=' . $type .
			' ORDER BY 
				ABS(wm3.meta_value) desc';
		
		$sql_litmit = $sql . ' limit ' . $offset . ',' . $limit;
		$query=$this->db->query($sql,false);
		$query_limit=$this->db->query($sql_litmit,false);
        $num_results = $query->num_rows();
		
		
		$pattern_img = '/^\[WPGP\sgif_id="(\d+)".*\]$/i';
		$pattern_video ='#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#';
		$replacement = '$1';
		
	    //get image
		foreach ($query_limit->result() as $row)
		{
			if ($row->url !=null) 
			{
				if (preg_match($pattern_img, $row->url)) {
				  $id_url= preg_replace($pattern_img, $replacement, $row->url);
				  $sql_img = "select guid from hai_posts where id =" . $id_url ;
				  $query_img=$this->db->query($sql_img,false);
				  $row->url= $query_img->result()[0]->guid;
				  
				}
				
				if (preg_match($pattern_video, $row->url,$matches)) {
					$row->url=$matches[0];
                    $row->image ="http://i.ytimg.com/vi/" . $matches[0] . "/0.jpg";
				}
				
			}
		}
        $kd['results'] = $query_limit->result();
        $kd['total'] = $num_results;
        if (!empty($kd)) {
            return ($kd);
        } else {
            return false;
        }
    }

    
    function get_all()
    {
        $this->db->select('*');
        $this->db->where('post_status', 'publish');
        $this->db->order_by("ID", "desc");
        $query = $this->db->get($this->_db);
        if ($result = $query->result()) {
            return $result;
        } else {
            return false;
        }
    }

    function show($id)
    {
        if ($id) {
            $sql = "
                SELECT *
                FROM {$this->_db}
                WHERE ID = " . $this->db->escape($id) . "
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows()) {
                return $query->row_array();
            }
        }
        return false;
    }

    function insert($data = array())
    {
        $this->db->insert($this->_db, $data);
        if ($id = $this->db->insert_id()) {
            return $id;
        } else {
            return false;
        }
    }

    function update($id, $data)
    {
        $this->db->update($this->_db, $data, array('ID' => $id));
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function delete($id)
    {
        $this->db->delete($this->_db, array('ID' => $id));
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function get_a_video($id = NULL)
    {
        if ($id) {
            $sql = "
                SELECT
				us.display_name author,
				p1.ID as id,
				p1.post_title as title,
				p1.post_title as description,
				p1.post_content as url,
				p1.post_modified as created,
				wm5.guid image,
				0 as cid, 
				IFNULL(wm3.meta_value,0) as views,
				IFNULL(wm4.post_type,0) as typepost
				FROM 
					hai_posts p1
				LEFT JOIN 
					hai_postmeta wm1
					ON (
						wm1.post_id = p1.id 
						AND wm1.meta_value IS NOT NULL
						AND wm1.meta_key = '_thumbnail_id'              
					)
				left join 
					hai_users us
					on (
							p1.post_author =us.ID 
					 )
				left join 
					hai_postmeta wm3
					on 
					(
						wm3.post_id = p1.id 
					   AND wm3.meta_key = '_count-views_all'
					)
				left join
					(select object_id,1 as post_type from hai_term_relationships where term_taxonomy_id=
					(SELECT term_taxonomy_id FROM hai_term_taxonomy
					where term_id =(SELECT term_id FROM hai_terms where name ='post-format-image')))  wm4
				on (
					wm4.object_id=p1.id 
				)
				left join 
				(select id,guid from hai_posts) wm5
				on 
				(wm1.meta_value=wm5.id)
				WHERE
					p1.post_status='publish'
					AND p1.post_type='post'
				and p1.ID=" . $this->db->escape($id) .
			" ORDER BY 
				p1.post_date DESC";

                $query = $this->db->query($sql);
			
				$pattern_img = '/^\[WPGP\sgif_id="(\d+)".*\]$/i';
				$pattern_video ='#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#';
				$replacement = '$1';
				
				//get image
				foreach ($query->result() as $row)
				{
					if ($row->url !=null) 
					{
						if (preg_match($pattern_img, $row->url)) {
						  $id_url= preg_replace($pattern_img, $replacement, $row->url);
						  $sql_img = "select guid from hai_posts where id =" . $id_url ;
						  $query_img=$this->db->query($sql_img,false);
						  $row->url= $query_img->result()[0]->guid;
						  
						}
						
						if (preg_match($pattern_video, $row->url,$matches)) {
							$row->url=$matches[0];
                            $row->image ="http://i.ytimg.com/vi/" . $matches[0] . "/0.jpg";
						}
						
					}
				}

            if ($query->num_rows()) {
                return $query->first_row();
            }
        }
        return FALSE;
    }


//    hide
    function title_exists($title)
    {
        $sql = "
            SELECT ID
            FROM {$this->_db}
            WHERE post_title = " . $this->db->escape($title) . "
            LIMIT 1
        ";
        $query = $this->db->query($sql);

        if ($query->num_rows()) {
            return TRUE;
        }
        return FALSE;
    }
    function url_exists($url)
    {
        $sql = "
            SELECT ID
            FROM {$this->_db}
            WHERE post_url = " . $this->db->escape($url) . "
            LIMIT 1
        ";
        $query = $this->db->query($sql);

        if ($query->num_rows()) {
            return TRUE;
        }
        return FALSE;
    }
}