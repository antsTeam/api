<div id="loginbox" class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php echo form_open(); ?>
            <div class="form-group">
                <?php echo form_label('Username or Email:') ?>
                <?php echo form_input('username', '', 'class="form-control"') ?>
            </div>
            <div class="form-group">
                <?php echo form_label('Password:') ?>
                <?php echo form_password('password', '', 'class="form-control"') ?>
            </div>
            <div class="checkbox">
                <label for="">
                    <?php echo form_checkbox('remember', 'accept') ?>
                    Remember me.
                </label>
            </div>
            <div class="form-group">
                <?php echo form_submit('login_submit', 'Login', 'class="btn btn-primary"') ?>
            </div>
            <div class="form-group">
                <div class="has-error">
                    <?php echo validation_errors(); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>