<?php if (validation_errors()): ?>
    <div class="alert alert-danger" role="alert"><?= validation_errors() ?></div>
<?php endif; ?>
<?= form_open(); ?>
    <div class="form-group form-group-sm">
        <?= form_label('Title:') ?>
        <?= form_input('title', isset($title) ? $title : '', 'class="form-control"') ?>
    </div>
    <div class="form-group form-group-sm">
        <?= form_label('Video Link:') ?>
        <?= form_input('url', isset($url) ? $url : '', 'class="form-control"') ?>
    </div>
    <div class="form-group form-group-sm">
        <?= form_label('Image link:') ?>
        <?= form_input('image', isset($image) ? $image : '', 'class="form-control"') ?>
    </div>
    <div class="form-group form-group-sm">
        <?= form_label('Description:') ?>
        <?= form_textarea('description', isset($description) ? $description : '', 'class="form-control input-small"') ?>
    </div>
    <div class="form-group form-group-sm">
        <?= form_submit("accept", "Update", 'class="btn btn-danger"') ?>
    </div>
<?= form_close(); ?>