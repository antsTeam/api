<table class="table">
    <thead>
    <tr>
        <td>#</td>
        <td>Title</td>
        <td>Created</td>
        <td>Status</td>
        <td>Operations</td>
    </tr>
    </thead>
    <tbody>
    <?php if (isset($list) && !empty($list)): $i=0; ?>
        <?php foreach ($list as $l): $i++?>
            <tr>
                <td><?=$i?></td>
                <td><a href="<?= base_url('admin/' . $modules . '/update/' . $l->id) ?>"><?= $l->title ?></a></td>
                <td><?= $l->created ?></td>
                <td><?= ($l->status == 1) ? 'Active' : 'Inactive' ?></td>
                <td><a href="<?= base_url('admin/' . $modules . '/delete/' . $l->id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure?')">Delete</a>
                </td>
            </tr>
        <?php endforeach;
    else: ?>
        <tr>
            <td colspan="5">
                No have any data!
            </td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
