<?php 
class leek
{
	private $s;
	private $p;
	private $c;

	public function __construct()
	{
		$this->s = false;
		$this->p = 0;
		$this->c = 0;
	}
	
	public function setSource($source = '')
	{
		$this->s = $source;
		$this->p = 0; 
		$this->c = strlen($source);
	}
	
	public function find($begin_with = '')
	{
		if($this->s && $this->p < $this->c)
		{
			$p = strpos($this->s, $begin_with, $this->p);
			
			if($p !== false)
			{
				$this->p = $p + strlen($begin_with);
				return true;
			}
			$this->p = $this->c;
		}
		return false;
	}
	
	public function findField($begin_with = '', $end_with = '')
	{
		if($this->s && $this->p < $this->c)
		{
			$b = strpos($this->s, $begin_with, $this->p);
			
			if($b !== false)
			{
				$b += strlen($begin_with);
				$e = strpos($this->s, $end_with, $b);
				
				if($e !== false && $e >= $b)
				{
					$this->p = $e;
					return substr($this->s, $b, $e - $b);	
				}
			}
		}
		return false;
	}
	public function findFieldReset($begin_with = '', $end_with = '')
	{
		$data = $this->findField ($begin_with, $end_with);
		$this->p =0;
		return $data;
	}
	
}